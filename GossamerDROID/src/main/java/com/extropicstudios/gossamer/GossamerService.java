package com.extropicstudios.gossamer;

import com.extropicstudios.gossamer.ledger.LedgerEntry;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;

public interface GossamerService {
	@GET("/users/{user}/ledger")
	List<LedgerEntry> listEntries(@Path("user") String user);
}
