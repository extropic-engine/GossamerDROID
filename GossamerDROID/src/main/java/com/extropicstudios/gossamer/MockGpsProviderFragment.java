package com.extropicstudios.gossamer;

import android.app.Fragment;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.Context.LOCATION_SERVICE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.location.LocationManager.GPS_PROVIDER;

/**
 * copy-pasted from https://stackoverflow.com/questions/7071625/using-a-mock-location-in-navigation-app
 * <p>
 * TODOs
 * <p>
 * - didn't have the layout code so i need to reverse engineer that
 * - move this into a drawer fragment instead of an activity
 */
public class MockGpsProviderFragment extends Fragment implements LocationListener {

    private MockGpsProvider mMockGpsProviderTask = null;
    private Integer mMockGpsProviderIndex = 0;

    private OnFragmentInteractionListener mListener;

    public static MockGpsProviderFragment newInstance() {
        MockGpsProviderFragment fragment = new MockGpsProviderFragment();
        fragment.setArguments(new Bundle());
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        String mocLocationProvider = GPS_PROVIDER;
        locationManager.addTestProvider(
                mocLocationProvider,
                false,
                false,
                false,
                false,
                true,
                false,
                false,
                0,
                5);
        locationManager.setTestProviderEnabled(mocLocationProvider, true);
        if (getActivity().checkSelfPermission(ACCESS_FINE_LOCATION) != PERMISSION_GRANTED
                && getActivity().checkSelfPermission(ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED
                ) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(mocLocationProvider, 0, 0, this);

        try {

            List<String> data = new ArrayList<String>();

            InputStream is = getActivity().getAssets().open("test.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = reader.readLine()) != null) {
                data.add(line);
            }

            // convert to a simple array so we can pass it to the AsyncTask
            String[] coordinates = new String[data.size()];
            data.toArray(coordinates);

            // create new AsyncTask and pass the list of GPS coordinates
            mMockGpsProviderTask = new MockGpsProvider();
            mMockGpsProviderTask.execute(coordinates);
        } catch (Exception ignored) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gpshack, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (OnFragmentInteractionListener) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // stop the mock GPS provider by calling the 'cancel(true)' method
        try {
            mMockGpsProviderTask.cancel(true);
            mMockGpsProviderTask = null;
        } catch (Exception ignored) {
        }

        // remove it from the location manager
        try {
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
            locationManager.removeTestProvider(MockGpsProvider.GPS_MOCK_PROVIDER);
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        // show the received location in the view
        TextView view = getActivity().findViewById(R.id.text);
        view.setText("index:" + mMockGpsProviderIndex
                + "\nlongitude:" + location.getLongitude()
                + "\nlatitude:" + location.getLatitude()
                + "\naltitude:" + location.getAltitude());
    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }


    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }


    /**
     * Define a mock GPS provider as an asynchronous task of this Activity.
     */
    private class MockGpsProvider extends AsyncTask<String, Integer, Void> {
        static final String LOG_TAG = "GpsMockProvider";
        static final String GPS_MOCK_PROVIDER = "GpsMockProvider";

        /**
         * Keeps track of the currently processed coordinate.
         */
        Integer index = 0;

        @Override
        protected Void doInBackground(String... data) {
            // process data
            for (String str : data) {
                // skip data if needed (see the Activity's savedInstanceState functionality)
                if (index < mMockGpsProviderIndex) {
                    index++;
                    continue;
                }

                // let UI Thread know which coordinate we are processing
                publishProgress(index);

                // retrieve data from the current line of text
                Double latitude;
                Double longitude;
                Double altitude;
                try {
                    String[] parts = str.split(",");
                    latitude = Double.valueOf(parts[0]);
                    longitude = Double.valueOf(parts[1]);
                    altitude = Double.valueOf(parts[2]);
                } catch (NullPointerException e) {
                    break;
                }        // no data available
                catch (Exception e) {
                    continue;
                }                // empty or invalid line

                // translate to actual GPS location
                Location location = new Location(GPS_PROVIDER);
                location.setLatitude(latitude);
                location.setLongitude(longitude);
                location.setAccuracy(16F);
                location.setAltitude(altitude);
                location.setTime(System.currentTimeMillis());
                location.setBearing(0F);

                // show debug message in log
                Log.d(LOG_TAG, location.toString());

                // provide the new location
                LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
                locationManager.setTestProviderLocation(GPS_PROVIDER, location);

                // sleep for a while before providing next location
                try {
                    Thread.sleep(200);

                    // gracefully handle Thread interruption (important!)
                    if (Thread.currentThread().isInterrupted())
                        throw new InterruptedException("");
                } catch (InterruptedException e) {
                    break;
                }

                // keep track of processed locations
                index++;
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            Log.d(LOG_TAG, "onProgressUpdate():" + values[0]);
            mMockGpsProviderIndex = values[0];
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }
}
